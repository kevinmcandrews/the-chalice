<img src="Title.jpg"
     alt="Title image"
     style="float: center;" />

# The Chalice

This document outlines the entire game. It is a work in progress; hell it might always be, and that's a good thing. If you modify the game in any way that this outline is inaccurate or insufficiently describes the game, please update this outline :). Oh, and no spoilers in the outline, please.


## Player Controls 
1. In regular mode
    1. look around (mouse motion)
    2. jump (Space)
    3. interact (E)
    4. move forward, backward, left, right (W, A, S, D)
2. In terminal mode
    1. keyboard commands:
		1. help
		2. pull
		3. push
		4. edit
		5. progress

## Objective

The objective of the game is for the player to create a better free and open source clone of his favorite indie game. 
The incomplete code is available on a free server in Albania, so the server operations take a while. 
The player accesses terminals to complete portions of the code. There are a set number of code changes that need to be made. 
Progress is only made when the changes are saved by uploading them back to the Albanian server. 
Saving can be done after each code change, but saving disables the current terminal for a few minutes.
Coding will be the least of your worries once you learn of the ominous presence which is after you.




