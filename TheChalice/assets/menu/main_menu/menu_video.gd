extends VideoPlayer

onready var _intro_video := preload("res://assets/menu/main_menu/videos/intro_text.ogv")

func _ready():
	self.connect("finished", self, "_loop_video")
	self.set_process(false)
	self.set_physics_process(false)
	EventBus.connect("started_game", self, "_start_intro")

func _loop_video():
	self.play()

func _start_intro():
	if Progress.played:
		_start_game()
		return

	$Interface/Control.set_visible(false)
	self.set_stream(_intro_video)
	self.disconnect("finished", self, "_loop_video")
	self.connect("finished", self, "_start_game")
	self.play()

func _start_game():
	Progress.played = true
	get_node("/root/main").start_game()
