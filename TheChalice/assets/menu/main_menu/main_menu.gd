extends Control

var prev_mouse_mode : int
var paused : bool

onready var start_game := find_node("StartGame")
onready var quit := find_node("Quit")
onready var instructions := find_node("Instructions")
onready var instructions_popup := find_node("InstructionsPopup")

func _ready():
	Progress.clear_progress()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().paused = false
	start_game.connect("pressed", self, "_start_game_pressed")
	quit.connect("pressed", self, "_quit_pressed")
	Music.stop()
	Music.play_normal_music_from_start()

func _start_game_pressed():
	EventBus.emit_signal("started_game")

func _quit_pressed():
	get_tree().quit()