extends Control

#signal exit_inspector
signal collect

export(NodePath) onready var picture = get_node(picture) as TextureRect
export(NodePath) onready var description = get_node(description) as RichTextLabel

var collectible : Node

func _input(event):
	if event.is_action_pressed("interact", false):
		emit_signal("collect")

func set_up(node, meta):
#	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	collectible = node
	picture.set_texture(meta.image)
	description.set_bbcode("[center]" + meta.description + "[/center]")
