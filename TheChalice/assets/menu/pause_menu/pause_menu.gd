extends Control

var prev_mouse_mode : int
var paused : bool

onready var resume_button : BaseButton = find_node("Resume")
onready var quit_button : BaseButton = find_node("Quit")
onready var settings_button : BaseButton = find_node("Settings")
onready var back_button : BaseButton = find_node("Back")
onready var volume_up_button : BaseButton = find_node("VolumeUp")
onready var volume_down_button : BaseButton = find_node("VolumeDown")

onready var book_count_label : Label = find_node("BookCountLabel")
onready var volume_value : Label = find_node("VolumeValue")

onready var main_screen : Container = find_node("MainContainer")
onready var settings_screen : Container = find_node("SettingsContainer")

func _ready():
	visible = false
	resume_button.connect("pressed", self, "_resume")
	quit_button.connect("pressed", self, "_quit")
	settings_button.connect("pressed", self, "_settings")
	back_button.connect("pressed", self, "_main")
	volume_up_button.connect("pressed", self, "_volume_up")
	volume_down_button.connect("pressed", self, "_volume_down")
	volume_value.set_text(str(Settings.get_volume()))

func _input(event):
	if event.is_action_pressed("Pause"):
		if not paused:
			prev_mouse_mode = Input.get_mouse_mode()
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			paused = true
			get_tree().paused = true
			book_count_label.set_text("%d/%d" % [Progress.books_collected, Progress.total_books])
			visible = true
		else:
			_resume()

func _resume():
	Input.set_mouse_mode(prev_mouse_mode)
	paused = false
	get_tree().paused = false
	visible = false

func _quit():
	get_node("/root/main").go_to_menu()

func _settings():
	main_screen.visible = false
	settings_screen.visible = true

func _volume_up():
	Settings.set_volume(Settings.get_volume() + 1)
	volume_value.set_text(str(Settings.get_volume()))

func _volume_down():
	Settings.set_volume(Settings.get_volume() - 1)
	volume_value.set_text(str(Settings.get_volume()))

func _main():
	settings_screen.visible = false
	main_screen.visible = true

