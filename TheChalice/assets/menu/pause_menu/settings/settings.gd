extends Node

var MAX_VOLUME := 10
var MIN_VOLUME := 0

onready var _volume_increment := 5

onready var _lowest_volume := -20
onready var _multiple := 2.5

func set_volume(new_volume):
	_volume_increment = int(clamp(new_volume, MIN_VOLUME, MAX_VOLUME))
	_update_master_volume()
	

func get_volume():
	return _volume_increment


func _ready():
	_update_master_volume()

func _update_master_volume():
	if _volume_increment == 0:
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), true)
	else:
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), false)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), _lowest_volume + _volume_increment*_multiple)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
