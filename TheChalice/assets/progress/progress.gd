extends Node

export (int) var total_books := 4
export (int) var total_edits := 15

var unsaved_edits := 0
var saved_edits := 0
var books_collected := 0
var played := false

var _rng = RandomNumberGenerator.new()
var _edit_history : PoolStringArray
var _code_map : Dictionary
var _completed_tutorial : bool

func _ready():
	randomize()
	_rng.randomize()
	var file = File.new()
	file.open("res://assets/progress/codes.res", file.READ)
	_code_map = parse_json(file.get_as_text())
	file.close()
	clear_progress()

func get_next_edit():
	while unsaved_edits + saved_edits > len(_edit_history) - 1:
		_generate_next_edit()
	return _edit_history[saved_edits + unsaved_edits]
	
func complete_edit():
	if not _completed_tutorial:
		_completed_tutorial = true
		EventBus.emit_signal("completed_tutorial")

	unsaved_edits += 1

func clear_edits():
	_edit_history.resize(saved_edits + 1)
	unsaved_edits = 0

func save_edits():
	saved_edits += unsaved_edits
	unsaved_edits = 0
	if saved_edits >= total_edits:
		EventBus.emit_signal("completed_codes")

func collect_book():
	books_collected += 1

func is_books_collected():
	return books_collected >= total_books

func is_all_code_edited():
	return unsaved_edits + saved_edits >= total_edits

func is_all_code_pushed():
	return saved_edits >= total_edits

func is_tutorial_completed():
	return _completed_tutorial

func clear_progress():
	unsaved_edits = 0
	saved_edits = 0
	books_collected = 0
	_edit_history = []
	_completed_tutorial = false

func _generate_next_edit():
	var code_pointer := ((unsaved_edits + saved_edits) % total_edits) % len(_code_map)
	var quality := "good" if _rng.randi_range(1,total_books) <= books_collected else "bad"
	_edit_history.append(PoolStringArray(_code_map[str(code_pointer)][quality]).join("\n"))
