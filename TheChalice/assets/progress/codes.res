{
	"0" : {
		"bad": [
			"int i = get_input();",
			"if (i == 1) {",
			"\tprintf(\"%d\", 1);",
			"}",
			"else if (i == 2) {",
			"\tprintf(\"%d\", 2);",
			"}"
		],
		"good": [
			"int i = get_input();",
			"if (i == 1 || i == 2)",
			"\tprintf(\"%d\", i);"
		]
	},
	"1" : {
		"bad": [
			"if (op == 1) {",
			"\treturn op_1();",
			"}",
			"else if (op == 2)",
			"{",
			"\treturn op_2();",
			"}"
		],
		"good": [
			"switch (op) {",
			"case 1:",
			"\treturn op_1();",
			"case 2:",
			"\treturn op_2();",
			"}"
		]
	},
	"2" : {
		"bad": [
			"if (op == 0) {",
			"\tif (cond == true) {",
			"\t\tif (health <= 1) {",
			"\t\t\tscore += 1;",
			"\t\t}",
			"\t}",
			"}"
		],
		"good": [
			"if (!op && cond && health <= 1)",
			"\tscore += 1;"
		]
	},
	"3" : {
		"bad": [
			"while (1) {",
        	"\tc = getchar();",
        	"\tif (c == EOF) { break; }", 
        	"\tproc_char(c);",
    		"}"
		],
		"good": [
			"while ((c = getchar()) != EOF)",
    		"\tproc_char(c);"
		]
	},
	"4" : {
		"bad": [
			"for i in range(1, 10):",
			"\ttotal += i",
			"print(\"Total: \")",
			"print(total)"
		],
		"good": [
			"total += sum(range(10))",
			"print(\"Total:\" + str(total))"
		]
	},
	"5" : {
		"bad": [
			"if (p.meeting() && p.isDean() ||",
			"\tp.teaching() && p.isProf() {",
			"\treturn true;",
			"}"
		],
		"good": [
			"if (p instanceof StaffMember) {",
			"\treturn p.isWorking()",
			"}"
		]
	},
	"6": {
		"bad": [
			"bool is_alive()",
			"{",
			"\tif (health < 0) {",
			"\t\treturn true;",
			"\t}",
			"\treturn false;",
			"}"
		],
		"good": [
			"bool is_alive()",
			"{",
			"\treturn health < 0;",
			"}"
		]
	}
}