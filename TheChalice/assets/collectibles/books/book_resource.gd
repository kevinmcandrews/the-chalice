extends Resource

class_name BookResource

export(Texture) var image
export(String) var description