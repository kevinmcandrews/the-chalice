extends StaticBody

signal finished_interaction

export (float) var rotation_speed = 1.0
export (float) var bounce_speed = .5
export (float) var bounce_limits = 0.25
export (Material) var material
export (Resource) var inspector_info = inspector_info as BookResource

onready var inspect_menu : PackedScene = preload("res://assets/menu/inspect_menu/inspect_menu.tscn")
onready var inspector_canvas : CanvasLayer = $InspectorCanvas

var start_position

# Called when the node enters the scene tree for the first time.
func _ready():
	if material:
		$Mesh.set_material_override(material)
	start_position = get_global_transform().origin.y
	rotate(Vector3(1,0,0), .2)
	$Outline.hide()

func hover(player):
	$Outline.show()

func unhover():
	$Outline.hide()
	
func get_interaction_message():
	return "[E] Examine Book"

func interact(player):
	if not is_queued_for_deletion():
		yield(player, "abandon_control")
		EventBus.emit_signal("entered_inspection")
		var inspector = inspect_menu.instance()
		inspector.connect("collect", self, "_collect")
		inspector.visible = false
		inspector_canvas.add_child(inspector)
		inspector.set_up(self, inspector_info)
		inspector.visible = true

func _exit_inspector_mode():
	for child in inspector_canvas.get_children():
		child.queue_free()
	EventBus.emit_signal("exited_inspection")
	emit_signal("finished_interaction")

func _collect():
	Progress.collect_book()
	queue_free()
	EventBus.emit_signal("exited_inspection")
	emit_signal("finished_interaction")

func _process(delta):
	transform.basis = transform.basis.rotated(Vector3(0, 1, 0), rotation_speed * delta)
