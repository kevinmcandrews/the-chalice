extends Node

var _active_buffer := 0

onready var _normal : AudioStream = load("res://assets/audio/music/hannya_normal.ogg")
onready var _fast : AudioStream = load("res://assets/audio/music/hannya_fast.ogg")
onready var _game_over : AudioStream = load("res://assets/audio/music/game_over.ogg")
onready var _buffers := [$Buffer0, $Buffer1]
onready var _crossfade := $CrossFade
onready var _crossfade2 := $CrossFade2
onready var _last_stream

var pause_times = {}

func _ready():
	$Buffer0.volume_db = -80
	$Buffer1.volume_db = -80

func switch_to_fast_music():
	_switch_to_track(_fast, 0.05)

func switch_to_normal_music(from_start : bool):
	_switch_to_track(_normal, 5.0, -3)

func switch_to_game_over_music():
	_switch_to_track(_game_over, 0.5)

func stop():
	$Buffer0.volume_db = -80
	$Buffer1.volume_db = -80
	_active_buffer = 0
	_last_stream = null
	pause_times = {}

func _switch_to_track(stream : AudioStream, duration : float, target_volume := 0):
	if _active_buffer >= 0 and not _buffers[_active_buffer].stream == stream:
		var inactive_buffer : int = (_active_buffer + 1) % len(_buffers)
		_buffers[inactive_buffer].stream = stream
		if stream in pause_times:
			_buffers[inactive_buffer].play(pause_times[stream])
		else:
			_buffers[inactive_buffer].play()
		_crossfade.interpolate_property(_buffers[_active_buffer], "volume_db", _buffers[_active_buffer].volume_db, -80, duration, 1, Tween.EASE_IN, 0)
		_crossfade.interpolate_property(_buffers[inactive_buffer], "volume_db", -80, target_volume, duration, 1, Tween.EASE_IN, 0)
		pause_times[_buffers[_active_buffer].stream] = _buffers[_active_buffer].get_playback_position()
		_crossfade.start()
		_active_buffer = inactive_buffer