extends Node

var music_player_scene = load("res://assets/audio/music/music_player.tscn")

var _music_player

# Called when the node enters the scene tree for the first time.
func _ready():
	_music_player = music_player_scene.instance()
	add_child(_music_player)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func play_normal_music():
	_music_player.switch_to_normal_music(false)

func play_normal_music_from_start():
	_music_player.switch_to_normal_music(true)

func play_fast_music():
	_music_player.switch_to_fast_music()

func play_game_over_music():
	_music_player.switch_to_game_over_music()

func stop():
	_music_player.stop()
