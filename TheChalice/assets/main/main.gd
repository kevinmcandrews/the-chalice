extends Node

onready var game : PackedScene = preload("res://assets/levels/town/town.tscn")
onready var main_menu : PackedScene = preload("res://assets/menu/main_menu/main_menu.tscn")
onready var end_scene : PackedScene = preload("res://assets/cutscenes/ending/ending.tscn")

func _ready():
	replace_main_scene(main_menu)
	EventBus.connect("completed_game", self, "_complete_game")

func start_game():
	replace_main_scene(game)

func go_to_menu():
	replace_main_scene(main_menu)

func _complete_game():
	replace_main_scene(end_scene)

func replace_main_scene(scene):
	call_deferred("change_scene", scene)

func change_scene(resource : Resource):
	var scene = resource.instance()
	
	for child in get_children():
		remove_child(child)
		child.queue_free()
	add_child(scene)
