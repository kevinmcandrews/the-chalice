extends VideoPlayer

var _scenes : Array

onready var shared_scene = load("res://assets/cutscenes/ending/shared_end.ogv")
onready var good_scene = load("res://assets/cutscenes/ending/good_end.ogv")
onready var bad_scene = load("res://assets/cutscenes/ending/bad_end.ogv")

func _ready():
	Music.stop()
	if Progress.is_books_collected():
		_scenes = [shared_scene, good_scene]
	else:
		_scenes = [shared_scene, bad_scene]
	connect("finished", self, "_finished")
	_finished()

func _finished():
	# No followup cutscene
	if not _scenes:
		get_node("/root/main").go_to_menu()

	set_stream(_scenes.pop_front())
	play()
