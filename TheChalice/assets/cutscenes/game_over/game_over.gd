extends Control


onready var _tween : Tween = $Tween

func _ready():
	visible = false

func show_game_over_transition():
	visible = true
	_tween.interpolate_property($Background, "color:a", 0.0, 1.0, 0.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	_tween.start()
	yield(_tween, "tween_completed")
	_tween.interpolate_property($Background, "color:a", 1.0, 0.0, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	_tween.start()

func show_game_over_screen():
	visible = true
	_tween.interpolate_property($Background, "color:a", 0.0, 1.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	_tween.start()
	yield(_tween, "tween_completed")
	yield(get_tree().create_timer(0.5), "timeout")
	$Message.visible = true
	yield(get_tree().create_timer(2.0), "timeout")
	get_node("/root/main").go_to_menu()
