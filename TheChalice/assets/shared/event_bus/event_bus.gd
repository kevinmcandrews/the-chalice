extends Node

signal accessed_terminal()
signal left_terminal()

signal started_game()

signal completed_game()
signal completed_codes()
signal completed_tutorial()

signal entered_dialog()
signal exited_dialog()

signal entered_inspection()
signal exited_inspection()

signal player_attacked()