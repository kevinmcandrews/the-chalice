extends Node

func _ready():
	EventBus.connect("completed_tutorial", self, "_open_doors")

func _open_doors():
	$Tween.interpolate_property($LeftDoor, "translation", $LeftDoor.transform.origin, $LeftDoor.transform.origin + Vector3(0,0,-1), 2.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.interpolate_property($RightDoor, "translation", $RightDoor.transform.origin, $RightDoor.transform.origin + Vector3(0,0,1), 2.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.connect("tween_all_completed", self, "_delete_doors")
	$Tween.start()

func _delete_doors():
	$LeftDoor.queue_free()
	$RightDoor.queue_free()
