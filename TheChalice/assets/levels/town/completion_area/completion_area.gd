extends Area

func _ready():
    connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body):
    if "player" in body.get_groups():
        EventBus.emit_signal("completed_game")