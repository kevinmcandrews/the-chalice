extends Node

onready var _enemy = load("res://assets/characters/enemy/enemy.tscn").instance()

func _ready():
	EventBus.connect("completed_tutorial", self, "_spawn_enemy")

func _spawn_enemy():
	add_child(_enemy)
