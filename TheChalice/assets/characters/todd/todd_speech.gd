extends StaticBody

signal finished_interaction

export(Resource) var intro_dialogue = intro_dialogue as Dialogue
export(Resource) var blocked_dialogue = blocked_dialogue as Dialogue
export(Resource) var ending_dialogue = ending_dialogue as Dialogue

export(NodePath) onready var dialogue_box = get_node(dialogue_box) as BlockingDialogBox

onready var _current_dialogue : Dialogue = intro_dialogue

func _ready():
	EventBus.connect("completed_codes", self, "_allow_passage")
	EventBus.connect("completed_tutorial", self, "_move_to_temple")

func hover(_node):
	pass

func unhover():
	pass

func get_interaction_message():
	return "[E] Talk"

func interact(player):
	yield(player, "abandon_control")
	EventBus.emit_signal("entered_dialog")
	var prev_state = $"../.."._state
	$"../.."._state = $"../..".ActionState.SPEAKING
	var text_break = ""
	for chunk in _current_dialogue.chunks:
		dialogue_box.set_speaker("Todd-Kun")
		if text_break:
			dialogue_box.append_text(text_break, 0)
			yield(dialogue_box, "break_ended")

		dialogue_box.set_text(chunk, 20)
		text_break = "[break]"
	yield(dialogue_box, "box_hidden")
	$"../.."._state = prev_state
	EventBus.emit_signal("exited_dialog")
	emit_signal("finished_interaction")
	$AudioStreamPlayer.play()

func _move_to_temple():
	_current_dialogue = blocked_dialogue

func _allow_passage():
	_current_dialogue = ending_dialogue
