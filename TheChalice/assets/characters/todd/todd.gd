extends Spatial

enum ActionState {BLOCKING, NOT_BLOCKING, SPEAKING}

onready var player = $"../Player"

export var move_limit = 2
export var not_blocking_offset = 2

onready var _state = ActionState.BLOCKING

func _ready():
	EventBus.connect("completed_codes", self, "_allow_passage")
	EventBus.connect("completed_tutorial", self, "_move_to_temple")
	$Todd/AnimationPlayer.get_animation("ToddPose").set_loop(true)
	$Todd/AnimationPlayer.get_animation("ToddSpeakingPose").set_loop(true)
	$Todd/AnimationPlayer.play("ToddPose")

func _process(_delta):
	match _state:
		ActionState.BLOCKING:
			_block()
		ActionState.SPEAKING:
			_chill()
		ActionState.NOT_BLOCKING:
			_chill()

func _move_to_temple():
	global_transform.origin = $"../mido_loc_2".global_transform.origin

func _block():
	$Todd/AnimationPlayer.play("ToddPose")
	var player_pos = (self.to_local(player.global_transform.origin)).z
	$Todd.transform.origin.z = clamp(player_pos, -move_limit, move_limit)

func _chill():
	$Todd/AnimationPlayer.play("ToddSpeakingPose")

func _allow_passage():
	_state = ActionState.NOT_BLOCKING
	$Todd.transform.origin.z = not_blocking_offset