class_name Player
extends KinematicBody
# Controller for the player

signal abandon_control

const GRAVITY = -32.8
const MAX_SPEED = 10
const JUMP_SPEED = 12
const ACCEL = 2
const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

export var mouse_sensitivity = 0.05

var is_movement_processed = true

var _dir
var _last_interactable = null
var _vel = Vector3()
var _stride_length = 1.0

onready var _rotation_helper = $rotation_helper
onready var _camera = $rotation_helper/Camera
onready var _footsteps : AudioStreamPlayer3D = $Footsteps
onready var _footstep_timer : Timer = Timer.new()
onready var _last_footstep_location := translation

export var speed = 6
export var ground_acceleration = 4
export var air_acceleration = 2
var acceleration = ground_acceleration
export var jump_height = 4.5
export var gravity = 9.8
export var stick_amount = 10

var direction = Vector3()
var velocity = Vector3()
var movement = Vector3()
var gravity_vec = Vector3()
var grounded = true

func _ready():
	$InteractLabel.visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(_delta):
	if is_movement_processed:
		_stride_length = 0.5 + movement.length()/7
		if (translation.distance_to(_last_footstep_location) > _stride_length and
				is_on_floor()):
			_last_footstep_location = translation
			_footsteps.play()


func _physics_process(delta):
	if is_movement_processed:
		_process_input(delta)
		_process_movement(delta)
		_process_interaction(delta)

func _input(event):
	if not is_movement_processed:
		return

	if (event is InputEventMouseMotion and
			Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED):
		_rotation_helper.rotate_x(deg2rad(event.relative.y * mouse_sensitivity))
		self.rotate_y(deg2rad(event.relative.x * mouse_sensitivity * -1))

		var camera_rot = _rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -80, 80)
		_rotation_helper.rotation_degrees = camera_rot


func _process_input(_delta):
	direction = Vector3()
	direction.z = -Input.get_action_strength("movement_forward") + Input.get_action_strength("movement_backward")
	direction.x = -Input.get_action_strength("movement_left") + Input.get_action_strength("movement_right")
	direction = -direction.normalized().rotated(Vector3.UP, rotation.y)

func _process_movement(delta):
	if is_on_floor():
		gravity_vec = -get_floor_normal() * stick_amount
		acceleration = ground_acceleration
		grounded = true
	else:
		if grounded:
			gravity_vec = Vector3.ZERO
			grounded = false
		else:
			gravity_vec += Vector3.DOWN * gravity * delta
			acceleration = air_acceleration

	velocity = velocity.linear_interpolate(direction * speed, acceleration * delta)
	movement.z = velocity.z + gravity_vec.z
	movement.x = velocity.x + gravity_vec.x
	movement.y = gravity_vec.y
	
	move_and_slide(movement, Vector3.UP)

func _process_interaction(_delta):
	var collided = $rotation_helper/Camera/RayCast.get_collider()
	
	# Handle hovering over an interactable node
	if collided is Node and collided.is_in_group("interactable"):
		if collided != _last_interactable and _last_interactable != null:
			if is_instance_valid(_last_interactable):
				_last_interactable.call("unhover")
		_last_interactable = collided
		collided.call("hover", self)
		$InteractLabel.visible = true
		$InteractLabel.text = collided.get_interaction_message()
	elif _last_interactable != null and is_instance_valid(_last_interactable):
		_last_interactable.call("unhover")

	if collided == null:
		$InteractLabel.visible = false

	if collided != null and Input.is_action_just_pressed("interact"):
		interact_with_node(collided)

func interact_with_node(node):
	var yield_result = node.interact(self)
	if yield_result is GDScriptFunctionState:
		disable_movement_processing()
		$InteractLabel.visible = false
		emit_signal("abandon_control")
		yield(node, "finished_interaction")
		enable_movement_processing()

func disable_movement_processing():
	is_movement_processed = false

func enable_movement_processing():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	is_movement_processed = true

func _immobolize():
	$InteractLabel.visible = false
	is_movement_processed = false

func attack():
	EventBus.emit_signal("player_attacked")
	_immobolize()
	$GameOverScreen/Control.show_game_over_transition()

func _is_on_floor():
	return $playerfeet.is_colliding()

func _is_in_ground():
	return $playerfeetlow.is_colliding()

func die(__):
	$GameOverScreen/Control.show_game_over_screen()
