# Run.gd
extends EnemyState

export var terminal_chase_speed := 2.0

func enter(_msg := {}) -> void:
	enemy.target = _msg["player"]
	EventBus.connect("left_terminal", state_machine, "transition_to", ["Find"])
	enemy.teleport(false)

func update(delta: float) -> void:
	enemy.speed = terminal_chase_speed
	enemy.animation_player.play("Walk")
	enemy.animation_player.set_speed_scale(enemy.animation_speed * terminal_chase_speed)
	Music.play_normal_music()

func physics_update(delta: float) -> void:
	if enemy.target.is_in_group("player") and enemy.interact_area.overlaps_body(enemy.target):
		state_machine.transition_to("Attack", {"player": enemy.target})

	enemy.move(delta)

func exit() -> void:
	EventBus.disconnect("left_terminal", state_machine, "transition_to")
