# Run.gd
extends EnemyState

export var detect_speed := 6.0

var _player

func enter(_msg := {}) -> void:
	Music.play_fast_music()
	enemy.animation_player.connect("animation_finished", self, "_finished_anim")
	enemy.animation_player.play("Surprise")
	enemy.animation_player.set_speed_scale(enemy.animation_speed * detect_speed)
	enemy.animation_player.get_animation("Surprise").set_loop(false)

func physics_update(_delta: float) -> void:
	# Start finding if player is not nearby
	if !enemy.target.is_in_group("player") or !enemy.escape_area.overlaps_body(enemy.target):
		state_machine.transition_to("Find")

func _finished_anim(anim : String):
	if anim == "Surprise":
		state_machine.transition_to("Chase")

func exit() -> void:
	enemy.animation_player.disconnect("finished", state_machine, "transition_to")
