# Run.gd
extends EnemyState

var _player

func enter(_msg := {}) -> void:
	var player = _msg["player"]

	enemy.use_game_over_camera()
	Music.play_game_over_music()
	player.attack()
	enemy.animation_player.set_speed_scale(1)
	enemy.animation_player.play("GameOver")
	enemy.animation_player.connect("animation_finished", player, "die")
