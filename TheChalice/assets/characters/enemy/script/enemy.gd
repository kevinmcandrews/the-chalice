class_name Enemy
extends KinematicBody

export var draw_path : bool = OS.is_debug_build()


var path = []
var path_node = 0

var speed
var target : Node
var animation_speed := 0.5

var begin = Vector3()
var end = Vector3()
var m = SpatialMaterial.new()

var _path_expiration_timer := Timer.new()
var _path_expiration_interval := 0.5
var _angular_acceleration := 5.0

onready var fsm := $StateMachine
onready var nav = get_parent()
onready var animation_player : AnimationPlayer = $Enemy/AnimationPlayer
onready var rotation_tween : Tween = $RotationTween
onready var locate_area : Area = $DetectArea
onready var locate_cast : RayCast = $LocateRayCast
onready var escape_area : Area = $ChaseEscapeArea
onready var stalk_area : Area = $StalkArea
onready var stalk_escape_area : Area = $StalkEscapeArea
onready var interact_area : Area = $InteractArea
onready var _locate_timer : Timer = $LocateTimer
onready var spawns : Array = nav.find_node("SpawnPoints").get_children()

func _ready():
	$BlockingDialogBox.set_speaker("???")
	$BlockingDialogBox.append_text("Noooooo. You can't copy my game! I'm coming after you.", 10)
	target = $"../../Player" # TODO: make this better

	_path_expiration_timer.connect("timeout", self, "_on_path_expired")
	_path_expiration_timer.autostart = true
	_path_expiration_timer.wait_time = _path_expiration_interval

	EventBus.connect("entered_dialog", self, "_pause")
	EventBus.connect("exited_dialog", self, "_continue")
	EventBus.connect("entered_inspection", self, "_pause")
	EventBus.connect("exited_inspection", self, "_continue")

func states_ready():
	add_child(_path_expiration_timer)
	fsm.transition_to("TerminalChase", {"player": target})

func move(delta):
	if path_node < path.size():
		var direction = (path[path_node] - transform.origin)
		var norm = direction.normalized()
		var direction_to_path_node = direction
		direction_to_path_node.y = 0
		var dist_to_path_node = direction_to_path_node.length()
		if dist_to_path_node < 0.3:
			path_node += 1
		else:
			move_and_slide(direction.normalized() * speed, Vector3.UP)
			rotation.y = lerp_angle(rotation.y, atan2(-norm.x, -norm.z), delta * _angular_acceleration)

func use_game_over_camera():
	$Enemy/Camera/Camera.current = true

func teleport(_near : bool):
	spawns.sort_custom(self, "_target_dist_compare")
	var eligiblespawns := spawns.duplicate()
	eligiblespawns = eligiblespawns.slice(len(spawns) - 4, len(spawns) - 1)

	global_transform.origin = eligiblespawns[randi() % len(eligiblespawns)].get_global_transform().origin


func _target_dist_compare(a, b):
	var n : Vector3 = target.get_global_transform().origin
	if typeof(a) != typeof(b):
		return typeof(a) < typeof(b)
	else:
		return a.get_global_transform().origin.distance_to(n) < b.get_global_transform().origin.distance_to(n)

func _on_path_expired():
	path = nav.get_simple_path(get_translation(), target.get_translation())
	path_node = 0

	if draw_path:
		var im = get_node("../Draw")
		#im.set_material_override(m)
		im.clear()
		im.begin(Mesh.PRIMITIVE_POINTS, null)
		im.add_vertex(begin)
		im.add_vertex(end)
		im.end()
		im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		for x in path:
			im.add_vertex(x)
		im.end()

func _pause():
	set_process(false)
	set_physics_process(false)
	fsm.set_process(false)
	fsm.set_physics_process(false)

func _continue():
	set_process(true)
	set_physics_process(true)
	fsm.set_process(true)
	fsm.set_physics_process(true)
