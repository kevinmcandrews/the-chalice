# Run.gd
extends EnemyState

export var find_speed := 8.0

func enter(_msg := {}) -> void:
	enemy.target = _get_random_spawn_target()
	EventBus.connect("accessed_terminal", self, "_transition_to_terminal_chase")

func update(delta: float) -> void:
	enemy.speed = find_speed
	enemy.animation_player.play("Walk")
	enemy.animation_player.set_speed_scale(enemy.animation_speed * find_speed)
	Music.play_normal_music()

func physics_update(delta: float) -> void:
	# Start stalking if player is nearby
	for body in enemy.stalk_area.get_overlapping_bodies():
		if body.is_in_group("player"):
			var msg = {"player" : body}
			state_machine.transition_to("Stalk", msg)

	# Detect player if can be seen
	enemy.locate_cast.set_cast_to(enemy.locate_cast.to_local(enemy.target.get_global_transform().origin))
	var body = enemy.locate_cast.get_collider()
	if body and body.is_in_group("player") and enemy.locate_area.overlaps_body(body):
		state_machine.transition_to("Detect")

	# Arrived at target, switch target
	if enemy.interact_area.overlaps_area(enemy.target) or enemy.path.empty():
		enemy.target = _get_random_spawn_target()
		print("getting new target: " + enemy.target.name)

	# Hack to fix scenario where no path and constantly switching between 
	# different targets
	if enemy.path.empty():
		enemy.teleport(false)

	enemy.move(delta)

func exit() -> void:
	EventBus.disconnect("accessed_terminal", self, "_transition_to_terminal_chase")

func _transition_to_terminal_chase(player : Node) -> void:
	state_machine.transition_to("TerminalChase", {"player": player})

func _get_random_spawn_target() -> Node:
	return enemy.spawns[randi() % len(enemy.spawns)]