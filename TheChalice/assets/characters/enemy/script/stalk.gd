# Run.gd
extends EnemyState

export var stalk_speed := 3.0

func enter(_msg := {}) -> void:
	EventBus.connect("accessed_terminal", self, "_transition_to_terminal_chase")

	if _msg["player"] == null or !_msg["player"].is_in_group("player"):
		state_machine.transition_to("Find")

	enemy.target = _msg["player"]

func update(delta: float) -> void:
	enemy.speed = stalk_speed
	enemy.animation_player.play("Walk")
	enemy.animation_player.set_speed_scale(enemy.animation_speed * stalk_speed)
	Music.play_normal_music()

func physics_update(delta: float) -> void:
	# Start finding if player is not nearby
	if !enemy.stalk_escape_area.overlaps_body(enemy.target):
		state_machine.transition_to("Find")

	# Start chasing if player is seen
	if _can_detect_player():
		state_machine.transition_to("Detect")

	enemy.move(delta)

func exit() -> void:
	EventBus.disconnect("accessed_terminal", self, "_transition_to_terminal_chase")

func _can_detect_player() -> bool:
	enemy.locate_cast.set_cast_to(enemy.locate_cast.to_local(enemy.target.get_global_transform().origin))
	var body = enemy.locate_cast.get_collider()
	return body and body.is_in_group("player") and enemy.locate_area.overlaps_body(body) and enemy.escape_area.overlaps_body(body)
	
func _transition_to_terminal_chase(player : Node) -> void:
	if !_can_detect_player():
		state_machine.transition_to("TerminalChase", {"player": player})
