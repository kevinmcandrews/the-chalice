# Run.gd
extends EnemyState

export var chase_speed := 6.5
export var chase_anim_speed := 5.0

func enter(_msg := {}) -> void:
	Music.play_fast_music()

func update(delta: float) -> void:
	enemy.speed = chase_speed
	enemy.animation_player.play("NarutoRun")
	enemy.animation_player.set_speed_scale(enemy.animation_speed * chase_anim_speed)

func physics_update(delta: float) -> void:
	# Start finding if player is not nearby
	if !enemy.target.is_in_group("player") or !enemy.escape_area.overlaps_body(enemy.target):
		enemy.teleport(false)
		state_machine.transition_to("Find")

	if enemy.interact_area.overlaps_body(enemy.target):
		state_machine.transition_to("Attack", {"player": enemy.target})

	enemy.move(delta)