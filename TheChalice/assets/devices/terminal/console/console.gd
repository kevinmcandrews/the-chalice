# This is essentially a very sh*tty and minimal OS for the terminal that does 
# some basic program management where only one program can run at a time.
# Focus is on the last spawned program, and when that program closes, the focus
# returns to the shell.
#
# ¯\_(ツ)_/¯

extends Control

signal rebooted
signal exited

var shell_program
var spawned_program

# This is like a /usr/bin of available programs to spawn
var available_programs = {
	"shell": preload("res://assets/devices/terminal/console/shell_program.tscn"),
	"editor": preload("res://assets/devices/terminal/console/editor_program.tscn")
}

func _ready():
	EventBus.connect("player_attacked", self, "_on_exit")
	visible = false
	start()

func _on_exit_program(program_control):
	if program_control == shell_program:
		close_program(shell_program)
		exit_terminal()
	else:
		close_program(program_control)
		focus_shell()

func _on_spawn_program(program_to_spawn):
	assert(program_to_spawn in available_programs)
	spawned_program = available_programs[program_to_spawn].instance()
	open_program(spawned_program)
	unfocus_shell()

func exit_terminal():
	Progress.clear_edits()
	#EventBus.emit_signal("exit_terminal")

# A start is a pseudo-boot (i.e. each time the terminal is accesssed the
# terminal is in a just booted state)
func start():
	get_parent().visible = true
	visible = true
	shell_program = available_programs["shell"].instance()
	shell_program.connect("rebooted", self, "_on_reboot")
	shell_program.connect("exited", self, "_on_exit")
	open_program(shell_program)

func unfocus_shell():
	shell_program.hide()
	shell_program.set_process(false)
	shell_program.set_process_input(false)

func focus_shell():
	shell_program.show()
	shell_program.set_process(true)
	shell_program.set_process_input(true)
	shell_program.resume()

func close_program(program):
	program.disconnect("exit_program", self, "_on_exit_program")
	program.disconnect("spawn_program", self, "_on_spawn_program")
	program.queue_free()

func open_program(program):
	add_child(program)
	program.connect("exit_program", self, "_on_exit_program")
	program.connect("spawn_program", self, "_on_spawn_program")
	program.show()
	program.set_process(true)
	program.set_process_input(true)
	program.start_program()

func _on_reboot():
	Progress.clear_edits()
	emit_signal("rebooted")

func _on_exit():
	Progress.clear_edits()
	emit_signal("exited")
