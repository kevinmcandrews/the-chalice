extends Node

signal exit_program
signal spawn_program

const informational_text = "YOU ARE IN EDIT MODE. PRESS <CTRL>-C TO GO BACK TO SHELL. EDITS: %d"
const separator_text : String = "--------------------------------------------------------"

const completed_text = "All code completed!"

var code_to_copy : String
var line_number_to_copy : int

onready var edit_window : TextEdit = $Editor
onready var info_window : RichTextLabel = $Info
onready var input_window : TextEdit = $Input
onready var code_window : RichTextLabel = $RichTextLabel

func start_program():
	refresh_editor()
	input_window.grab_focus()

func _ready():
	input_window.connect("text_changed", self, "_on_text_changed")
	code_to_copy = Progress.get_next_edit()
	edit_window.hide()

func _process(delta):
	input_window.grab_focus()

func _input(event):
	if event.is_action_pressed("CloseEditor"):
		emit_signal("exit_program", self)
		return

	if event.is_action_pressed("RunCommand", true):
		get_tree().set_input_as_handled()
		if OS.is_debug_build():
			complete_line()
		return

	#if Progress.is_all_code_edited():
	#	get_tree().set_input_as_handled()

func _on_text_changed():
	var line_to_copy = edit_window.get_line(line_number_to_copy)
	var typed_code = input_window.get_line(0)
	
	if not line_to_copy.begins_with(typed_code):
		input_window.add_color_override("font_color", Color(1,0,0,1))
	else:
		input_window.add_color_override("font_color", Color(1,1,1,1))

	# Check for completion
	if typed_code == line_to_copy:
		complete_line()

func complete_line():
	if Progress.is_all_code_edited():
		return

	input_window.set_text("")
	line_number_to_copy += 1
	
	if (line_number_to_copy >= edit_window.get_line_count()):
		Progress.complete_edit()
		code_to_copy = Progress.get_next_edit()
		refresh_editor()

	refresh_code_window()

func refresh_editor():
	info_window.set_text(informational_text % [Progress.unsaved_edits] + 
			"\n" + separator_text)
	line_number_to_copy = 0
	input_window.set_text("")
	edit_window.set_text(code_to_copy)
	refresh_code_window()

func refresh_code_window():
	if Progress.is_all_code_edited():
		code_window.set_bbcode(completed_text)
		return

	var code_lines : PoolStringArray = code_to_copy.split("\n")
	for i in range(len(code_lines)):
		code_lines[i] = "[color=gray]%d.[/color] %s" % [i + 1, code_lines[i]]
		if i == line_number_to_copy:
			code_lines[i] = "[pulse color=#00FFAA height=0.0 freq=5.0]  [/pulse]" + code_lines[i]
		else:
			code_lines[i] = "  " + code_lines[i]
	code_window.set_bbcode(code_lines.join("\n"))
