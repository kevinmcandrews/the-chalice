extends CanvasLayer

export (float, 0 ,1) var hud_screen_percentage = .9
var viewport_size
var hud_size

func _ready():
	get_tree().get_root().connect("size_changed", self, "_on_view_size_changed")
	_on_view_size_changed()

func _on_view_size_changed():
	hud_size = $Console/ConsoleTexture.texture.get_size()
	viewport_size = get_viewport().get_visible_rect().size
	var view_to_hud_ratio = viewport_size/hud_size

	# Scale to keep aspect and fit on screen
	scale.x = hud_screen_percentage*view_to_hud_ratio.x
	scale.y = hud_screen_percentage*view_to_hud_ratio.x
	if (scale.y*hud_size.y > viewport_size.y):
		scale.x = view_to_hud_ratio.y
		scale.y = view_to_hud_ratio.y

	# Bottom Center the texture
	offset.x = (viewport_size.x - scale.x*hud_size.x)/2
	offset.y = (viewport_size.y - scale.y*hud_size.y)
