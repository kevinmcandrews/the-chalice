extends Control

signal rebooted
signal exited
signal exit_program
signal spawn_program

signal _input_event

const Commands = {
	"pull": {
		"function": "_on_pull_command",
		"help": "Retrieves the project code"
	},
	"exit": {
		"function": "_on_exit_command",
		"help": "Leaves the terminal -- WARNING: Unsaved edits will be lost"
	},
	"push": {
		"function": "_on_push_command",
		"help": "Saves the project code -- Requires a reboot; This is a known issue."
	},
	"edit": {
		"function": "_on_edit_command",
		"help": "Enters the editor for writing code"
	},
	"help": {
		"function": "_on_help_command",
		"help": "Displays a help menu that lists available commands"
	},
	"clear": {
		"function": "_on_clear_command",
		"help": "Clears the screen"
	},
	"progress": {
		"function": "_on_progress_command",
		"help": "Shows the number of (un)saved edits"
	},
}

const welcome_text = """WELCOME TO COMPUTER-CHAN. ENTER "help" TO SEE A LIST OF AVAILABLE COMMANDS"""
const help_text = """THE FOLLOWING COMMANDS ARE AVAILABLE:

exit, clear, help, pull, push, edit, progress

ENTER "<command> help" TO GET MORE INFORMATION ON A COMMAND"""


const prompt_text = "$ "
const MAX_LINES = 15
const MAX_CHARS = 50

# Whether the code has been pulled
var pulled = false

export (float) var pull_time = 2.0
export (float) var push_time = 2.0

var ready_for_input = false

var _command_parse_regex = RegEx.new()

onready var shell : TextEdit = $Shell

var press_count = 0

func _ready():
	shell.connect("cursor_changed", self, "_on_cursor_changed")
	shell.connect("text_changed", self, "_on_text_changed")
	_command_parse_regex.compile("\\S+") # negated whitespace character class

func _process(delta):
	shell.grab_focus()

func start_program():
	pulled = false
	shell.insert_text_at_cursor(welcome_text)
	shell.grab_focus()
	display_command_prompt()

func resume():
	shell.grab_focus()

func _input(event):
	# Ignore non-key events
	if not event is InputEventKey:
		get_tree().set_input_as_handled()
		return

	# if a command is in progress, pass input to it
	if not ready_for_input:
		get_tree().set_input_as_handled()
		emit_signal("_input_event", event)
		return

	if event.is_action_pressed("RunCommand", true):
		ready_for_input = false
		get_tree().set_input_as_handled()
		var call_state = run_command(parse_command())
		if not call_state == null:
			yield(call_state, "completed")
		display_command_prompt()
	elif (event.is_action_pressed("Backspace", true) and
				shell.cursor_get_column() <= len(prompt_text)):
		get_tree().set_input_as_handled()

func _on_cursor_changed():
	# prevent cursor from moving, by always just putting it at the end
	shell.cursor_set_line(999999)
	shell.cursor_set_column(999999)

func _on_text_changed():
	if shell.get_line_count() > MAX_LINES:
		var t = Array(shell.get_text().split("\n"))
		t = PoolStringArray(t.slice(len(t) - MAX_LINES, len(t) - 1))
		shell.set_text(t.join("\n")) 
	shell.set_v_scroll(INF)
	
func parse_command():
	var raw_line: String = shell.get_line(shell.cursor_get_line())
	raw_line = raw_line.lstrip(prompt_text).strip_edges()
	var commands = []
	for sub_c in _command_parse_regex.search_all(raw_line):
		commands.append(sub_c.get_string())
	return commands
	
func run_command(commands):
	if commands.empty():
		return
	
	var command = commands[0]
	if not command in Commands:
		shell.insert_text_at_cursor("\nInvalid Command")
	else:
		return call(Commands[command]["function"], commands)

func _on_exit_command(commands):
	if _std_help(commands):
		return
	
	emit_signal("exited")

func _std_help(commands):
	if len(commands) == 1:
		return false
	elif len(commands) == 2 and commands[1] == "help":
		shell.insert_text_at_cursor("\n%s" % [Commands[commands[0]]["help"]])
		return true
	else:
		shell.insert_text_at_cursor("\nInvalid usage of command: %s" % [commands[0]])
		return true

func _on_help_command(commands):
	if _std_help(commands):
		return
	
	shell.insert_text_at_cursor("\n" + help_text)

func _on_clear_command(commands):
	if _std_help(commands):
		return

	shell.set_text("")

func _on_pull_command(commands):
	if _std_help(commands):
		return

	if pulled:
		shell.insert_text_at_cursor("\nYou already have the latest changes")
	else:
		shell.insert_text_at_cursor("\nPulling...")
		yield(get_tree().create_timer(pull_time), "timeout")
		shell.insert_text_at_cursor("\nCode pulled successfully.")
		pulled = true

func _on_edit_command(commands):
	if _std_help(commands):
		return

	if not pulled:
		shell.insert_text_at_cursor("\nYou must pull the code first")
	else:
		emit_signal("spawn_program", "editor")

func _on_push_command(commands):
	if _std_help(commands):
		return

	if not pulled:
		shell.insert_text_at_cursor("\nYou must pull the code first")
	elif Progress.unsaved_edits == 0:
		shell.insert_text_at_cursor("\nNo unsaved edits to push")
	else:
		shell.insert_text_at_cursor("\nPushing...")
		yield(get_tree().create_timer(push_time), "timeout")
		Progress.save_edits()
		shell.insert_text_at_cursor("\nCode pushed successfully.\nReboot required.\nPress any key to reboot.")
		yield(self, "_input_event")
		emit_signal("rebooted")

func _on_progress_command(commands):
	if _std_help(commands):
		return

	if not pulled:
		shell.insert_text_at_cursor("\nYou must pull the code first")
	else:
		shell.insert_text_at_cursor("\nUnsaved Edits: %d\nSaved Edits: %d/%d" % [Progress.unsaved_edits, Progress.saved_edits, Progress.total_edits])

func display_command_prompt(var newline = true):
	if newline:
		shell.insert_text_at_cursor("\n" + prompt_text)
	else:
		shell.insert_text_at_cursor(prompt_text)
	ready_for_input = true
