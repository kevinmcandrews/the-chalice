extends Spatial

signal finished_interaction

onready var console_scene : PackedScene = load("res://assets/devices/terminal/console/console.tscn")

var down_message ="[E] Power on Terminal"
var booting_message ="Terminal Rebooting... %02d:%02d"
var up_message = "[E] Use Terminal"
var console : CanvasLayer

const State = {
	OFF = 0,
	BOOTING = 1,
	ON = 2
}

onready var current_message : String = up_message
onready var state : int = State.ON

var boot_timer

export (float) var boot_time = 180.0

func _ready():
	boot_timer = Timer.new()
	boot_timer.connect("timeout",self,"_on_booted")
	boot_timer.set_one_shot(true)
	add_child(boot_timer) #to process
	state = State.ON
	$Outline.hide()
	
func interact(player):
	if not $Area.overlaps_body(player):
		return
		
	match state:
		State.OFF:
			_boot_up()
		State.BOOTING:
			pass
		State.ON:
			yield(player, "abandon_control")
			console = console_scene.instance()
			console.get_node("Console/ConsoleProgramManager").connect("rebooted", self, "_reboot")
			console.get_node("Console/ConsoleProgramManager").connect("exited", self, "_stop")
			EventBus.emit_signal("accessed_terminal", player)
			add_child(console)

func is_on():
	return state == State.ON

func _stop():
	console.queue_free()
	EventBus.emit_signal("left_terminal")
	emit_signal("finished_interaction")

func hover(player):
	if not $Area.overlaps_body(player):
		$Outline.hide()
		current_message = ""
		return

	match state:
		State.OFF:
			current_message = down_message
		State.BOOTING:
			var t_s = int(boot_timer.get_time_left())
			current_message = booting_message % [t_s/60, t_s % 60]
		State.ON:
			current_message = up_message
	$Outline.show()

func unhover():
	$Outline.hide()

func _shutdown():
	state = State.OFF

func _reboot():
	_shutdown()
	_stop()
	_boot_up()

func _boot_up():
	state = State.BOOTING
	boot_timer.start(boot_time)

func _on_booted():
	state = State.ON

func get_interaction_message():
	return current_message

